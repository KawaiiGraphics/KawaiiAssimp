#ifndef AIMATKEYWRAPPER_HPP
#define AIMATKEYWRAPPER_HPP

#include <assimp/material.h>

#include <Kawaii3D/AssetZygote/KawaiiTextureRole.hpp>
#include <Kawaii3D/AssetZygote/KawaiiTextureParamRole.hpp>
#include <Kawaii3D/AssetZygote/KawaiiRenderParamRole.hpp>

#include <variant>
#include <tuple>

using AssimpTextureInfo = std::tuple<KawaiiTextureRole, KawaiiTextureParamRole, size_t>;
using AssimpParamKeyInfo = std::variant<KawaiiRenderParamRole, AssimpTextureInfo, std::monostate>;
AssimpParamKeyInfo trRenderParamKey(const std::string &key, int texType = 0, size_t idx = 0);

#endif // AIMATKEYWRAPPER_HPP
