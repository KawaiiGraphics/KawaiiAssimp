#ifndef KAWAIIASSIMPBONE_H
#define KAWAIIASSIMPBONE_H

#include <QString>
#include <vector>
#include <glm/mat4x4.hpp>
#include <Kawaii3D/Geometry/KawaiiBone3D.hpp>

class KawaiiAssimpMesh;
struct KawaiiAssimpBone
{
  KawaiiAssimpBone(const QString &name, const glm::mat4 &tr, std::vector<KawaiiBone3D::Influence> &&influence, KawaiiAssimpMesh *mesh);

  QString name;
  glm::mat4 tr;
  std::vector<KawaiiBone3D::Influence> influence;

  KawaiiAssimpMesh *mesh;
};

#endif // KAWAIIASSIMPBONE_H
