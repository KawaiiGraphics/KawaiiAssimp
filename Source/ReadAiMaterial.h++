#ifndef READAIMATERIAL_HPP
#define READAIMATERIAL_HPP

#include <Kawaii3D/AssetZygote/KawaiiMaterialZygote.hpp>
#include <assimp/material.h>
#include <QDir>

KawaiiMaterialZygote readAiMaterial(aiMaterial *material0, const QDir &dir);

#endif // READAIMATERIAL_HPP
