#include "ReadAiMaterial.h++"
#include "AiMatkeyWrapper.h++"
#include "Util.h++"

namespace {
  template<typename T, typename Func>
  void invokeSetZygoteParam(Func &&func, const T *values, size_t bufSz)
  {
    Q_ASSERT_X(bufSz % sizeof(T) == 0, "parse assimp DOM", "param size missmatch");

    size_t arrSize = bufSz / sizeof(T);
    switch(arrSize)
      {
      case 1:
        func(*values);
        break;

      case 2:
        func(*reinterpret_cast<const glm::vec<2, T, glm::defaultp>*>(values));
        break;

      case 3:
        func(*reinterpret_cast<const glm::vec<3, T, glm::defaultp>*>(values));
        break;

      case 4:
        func(*reinterpret_cast<const glm::vec<4, T, glm::defaultp>*>(values));
        break;

      default:
        qWarning("Ooops! Matrix?");
      }
  }

  template<typename Func>
  void readAiParam(aiMaterialProperty *param0, Func &&func_set)
  {
    switch(param0->mType)
      {
      case aiPTI_Float:
        invokeSetZygoteParam(func_set, reinterpret_cast<float*>(param0->mData), param0->mDataLength);
        break;

      case aiPTI_Double:
        invokeSetZygoteParam(func_set, reinterpret_cast<double*>(param0->mData), param0->mDataLength);
        break;

      case aiPTI_String:
        {
          const char *str_begin = param0->mData+4,
              *str_end = param0->mData + param0->mDataLength - 1;

          std::string str(str_begin, str_end);
//          material0->Get(param0->mKey.C_Str(), param0->mSemantic, param0->mIndex, str);
          func_set(str /*util::stdStr(str)*/);
        }
        break;

      case aiPTI_Integer:
        invokeSetZygoteParam(func_set, reinterpret_cast<int32_t*>(param0->mData), param0->mDataLength);
        break;

      case aiPTI_Buffer:
        func_set(std::vector<std::byte>(reinterpret_cast<std::byte*>(param0->mData),
                                    reinterpret_cast<std::byte*>(param0->mData) + param0->mDataLength));
        break;

      default: break;
      }
  }
}

KawaiiMaterialZygote readAiMaterial(aiMaterial *material0, const QDir &dir)
{
  KawaiiMaterialZygote result;

  for(auto j = material0->mProperties; j < material0->mProperties + material0->mNumProperties; ++j)
    {
      auto param0 = *j;
      auto param = trRenderParamKey(util::stdStr(param0->mKey), param0->mSemantic, param0->mIndex);

      if(std::holds_alternative<KawaiiRenderParamRole>(param))
        {
          auto func = [&result, param](auto &&value)
          { result.setParam(std::get<KawaiiRenderParamRole>(param), value); };

          readAiParam(param0, func);
        } else
        if(std::holds_alternative<AssimpTextureInfo>(param))
          {
            auto &renderTexParam = std::get<AssimpTextureInfo>(param);

            KawaiiTextureRole texRole = std::get<0>(renderTexParam);
            size_t idx = std::get<2>(renderTexParam);
            KawaiiTextureParamRole paramKey = std::get<1>(renderTexParam);

            auto func = [&result, texRole, paramKey, idx](auto &&value)
            { result.setTextureParam(texRole, idx, paramKey, value); };

            readAiParam(param0, func);
          }
    }

  auto makeAbsolute = [&dir] (QString &path) {
    path.replace('\\', '/');
    QString origPath = path;
    if(!QFile::exists(path))
      path = dir.absoluteFilePath(path);

    if(!QFile::exists(path))
      path = dir.absoluteFilePath(path.trimmed());

    if(!QFile::exists(path))
      path = dir.absoluteFilePath(path.right(path.size() - path.lastIndexOf('/') - 1));

    if(!QFile::exists(path))
      path = origPath;
  };

  result.forallTexturesParam<QString>(KawaiiTextureParamRole::FileName, makeAbsolute);

  return result;
}
