#ifndef KAWAIIASSIMPMESH_HPP
#define KAWAIIASSIMPMESH_HPP

#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include "KawaiiAssimp_global.hpp"
#include "KawaiiAssimpNode.h++"

struct aiMesh;

class KawaiiAssimpMesh : public KawaiiMesh3D
{
public:
  enum ImportWarnings
  {
    CustomVertexAttributes = 1,
    UnsupportedPolygonType = 2
  };

  KawaiiAssimpMesh(aiMesh *origin, uint8_t &warningFlags);

  void transform(const QMatrix4x4 &trMat, const QMatrix4x4 &normMat);

  void loadBones(const std::unordered_map<std::string, KawaiiAssimpNode*> &allNodes);

private:
  aiMesh *origin;
};

#endif // KAWAIIASSIMPMESH_HPP
