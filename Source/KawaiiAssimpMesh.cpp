#include "KawaiiAssimpMesh.h++"
#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <QMatrix4x4>
#include "Util.h++"
#include <queue>

using namespace util;

KawaiiAssimpMesh::KawaiiAssimpMesh(aiMesh *origin, uint8_t &warningFlags):
  KawaiiMesh3D(),
  origin(origin)
{
  if(!(warningFlags & CustomVertexAttributes) &&
     (origin->HasTangentsAndBitangents() || origin->HasVertexColors(0)))
    {
      qWarning("KawaiiAssimp: custom attributes not implemented yet. Loaded scene may be incomplete!");
      warningFlags |= CustomVertexAttributes;
    }

  setObjectName(qStr(origin->mName));
  for(unsigned j = 0; j < origin->mNumVertices; ++j)
    {
      auto &vertex = addVertex(qVec4(origin->mVertices[j]));

      if(origin->HasNormals())
        vertex.normalRef() = qVec3(origin->mNormals[j]);

      if(origin->HasTextureCoords(0))
        vertex.texCoordRef() = qVec3(origin->mTextureCoords[0][j]);
    }

  for(unsigned j = 0; j < origin->mNumFaces; ++j)
    {
      const auto &face = origin->mFaces[j];
      if(face.mNumIndices < 3)
        {
          if(!(warningFlags & UnsupportedPolygonType))
            {
              qCritical("KawaiiAssimp: only triangles and polygons are supported! Missing geometry!");
              warningFlags |= UnsupportedPolygonType;
            }
          continue;
        }
      for(size_t i = 0; i < face.mNumIndices - 1; i += 2)
        {
          size_t third = i + 2;
          if (third >= face.mNumIndices)
            third = 0;
          addTriangle(face.mIndices[i], face.mIndices[i + 1], face.mIndices[third]);
        }
    }

  if(!origin->HasNormals())
    computeNormals();

  if(!origin->HasTextureCoords(0))
    computeTexCoordsSpheral();
}

void KawaiiAssimpMesh::transform(const QMatrix4x4 &trMat, const QMatrix4x4 &normMat)
{
  forallVerticesP([&trMat, &normMat] (KawaiiPoint3D &vert) {
    vert.positionRef() = trMat * vert.positionRef();
    vert.normalRef() = normMat.map(vert.normalRef());
    }).waitForFinished();
}

void KawaiiAssimpMesh::loadBones(const std::unordered_map<std::string, KawaiiAssimpNode*> &allNodes)
{
  for(uint32_t i = 0; i < origin->mNumBones; ++i)
    {
      auto *bone = origin->mBones[i];
      const auto boneName = stdStr(bone->mName);

      std::vector<KawaiiBone3D::Influence> influence;
      influence.reserve(bone->mNumWeights);
      for(uint32_t j = 0; j < bone->mNumWeights; ++j)
        influence.push_back({bone->mWeights[j].mVertexId, bone->mWeights[j].mWeight});

      if(auto el = allNodes.find(boneName); el != allNodes.end())
        el->second->bones.emplace_back(QString::fromStdString(boneName), trMat(bone->mOffsetMatrix), std::move(influence), this);
    }
}
