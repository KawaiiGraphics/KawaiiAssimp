#include "KawaiiAssimpModelLoader.h++"
#include "KawaiiAssimpLoader.hpp"
#include "KawaiiAssimpMesh.h++"
#include "ReadAiMaterial.h++"
#include "Util.h++"

#include <glm/gtc/matrix_transform.hpp>

#include <Kawaii3D/Exceptions/KawaiiIOExcep.hpp>
#include <Kawaii3D/KawaiiCamera.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

#include <sib_utils/ioReadAll.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#ifdef ASSIMP_EARLY_LINK
#include <assimp/cimport.h>
#else
#include <QLibrary>
#include <iostream>

namespace {
  QLibrary& getAssimpLib()
  {
    static std::unique_ptr<QLibrary> holder;
    static bool firstRun = true;
    auto tryLoadLib = [] (const QString &libname) {
        if(!holder)
          {
            if(QLibrary::isLibrary(libname))
              holder = std::make_unique<QLibrary>(libname);
            if(holder && !holder->load())
              {
                std::cerr << "KawaiiAssimp: loading backend \"" << holder->fileName().toStdString() << "\" failed: " << holder->errorString().toStdString() << std::endl;
                holder.reset();
              }
          }
      };
    if(firstRun)
      {
#       ifndef Q_OS_WINDOWS
        tryLoadLib(QStringLiteral("libassimp.so"));
        tryLoadLib(QStringLiteral("libassimp.dylib"));
#       endif
        tryLoadLib(QStringLiteral("libassimp.dll"));
        tryLoadLib(QStringLiteral("assimp.dll"));
        tryLoadLib(QStringLiteral("assimp"));
      }
    static QLibrary nullLib;
    return holder? *holder: nullLib;
  }
}

const aiScene* aiImportFile(const char* pFile, unsigned int pFlags)
{
  static const aiScene* (*fn) (const char* pFile, unsigned int pFlags) = nullptr;
  if(!fn)
    fn = reinterpret_cast<decltype(fn)>(getAssimpLib().resolve("aiImportFile"));

  if(fn)
    return fn(pFile, pFlags);
  else
    return nullptr;
}

const aiScene* aiImportFileFromMemory(const char* pBuffer, unsigned int pLength, unsigned int pFlags, const char* pHint)
{
  static const aiScene* (*fn) (const char* pBuffer, unsigned int pLength, unsigned int pFlags, const char* pHint) = nullptr;
  if(!fn)
    fn = reinterpret_cast<decltype(fn)>(getAssimpLib().resolve("aiImportFileFromMemory"));

  if(fn)
    return fn(pBuffer, pLength, pFlags, pHint);
  else
    return nullptr;
}

void aiReleaseImport(const aiScene* pScene)
{
  if(pScene)
    {
      static void (*fn) (const aiScene* pScene) = nullptr;
      if(!fn)
        fn = reinterpret_cast<decltype(fn)>(getAssimpLib().resolve("aiReleaseImport"));

      if(fn)
        fn(pScene);
    }
}

const char *aiGetErrorString()
{
  static const char* (*fn) () = nullptr;
  static constexpr const char unknownErrorStr[] = "libassimp is not loaded!";

  if(!fn)
    fn = reinterpret_cast<decltype(fn)>(getAssimpLib().resolve("aiGetErrorString"));

  if(fn)
    return fn();
  else
    return unknownErrorStr;
}
#endif
using namespace util;

namespace {
  QDir getDir(const QString &path)
  {
    QDir d(path);
    d.cdUp();
    return d.absolutePath();
  }

  const aiScene *sceneFromIo(QIODevice &io)
  {
    static constexpr unsigned assimpFlags = aiProcess_ValidateDataStructure
        | aiProcess_GenUVCoords | aiProcess_FlipUVs
        | /*aiProcess_JoinIdenticalVertices |*/ aiProcess_Triangulate | aiProcess_OptimizeMeshes
        | aiProcess_FindInvalidData | aiProcess_LimitBoneWeights;

    if(QFile *f = dynamic_cast<QFile*>(&io); f)
      {
        const QByteArray fName = QFile::encodeName(f->fileName());
        const aiScene *sc = aiImportFile(fName.constData(), assimpFlags);
        if(sc)
          return sc;
      }

    const QByteArray buf = sib_utils::ioReadAll(io, QIODevice::ReadOnly);
    const aiScene *sc = aiImportFileFromMemory(buf.constData(), buf.length(), assimpFlags, NULL);
    if(!sc)
      {
        qCritical() << "KawaiiAssimp:error: Failed to import" << &io;
        qCritical() << aiGetErrorString();
      }
    return sc;
  }
}

KawaiiAssimpLoader::KawaiiAssimpLoader(QIODevice &io, const QDir &dataDir):
  dataDir(dataDir),
  scene(sceneFromIo(io))
{
}

KawaiiAssimpLoader::KawaiiAssimpLoader(QIODevice &io):
  scene(sceneFromIo(io))
{
  if(auto file = dynamic_cast<QFile*>(&io); file)
    dataDir = getDir(file->fileName());
  else
    dataDir = QDir::current();
}

KawaiiAssimpLoader::KawaiiAssimpLoader(QIODevice &&io):
  KawaiiAssimpLoader(static_cast<QIODevice&>(io))
{ }

KawaiiAssimpLoader::KawaiiAssimpLoader(const QString &fileName):
  KawaiiAssimpLoader(QFile(fileName))
{ }

KawaiiAssimpLoader::~KawaiiAssimpLoader()
{
  if(scene)
    aiReleaseImport(scene);
}

bool KawaiiAssimpLoader::check() const
{
  return scene;
}

bool KawaiiAssimpLoader::load(KawaiiDataUnit *target, const QMatrix4x4 &trMat)
{
  if(auto *mesh = dynamic_cast<KawaiiMesh3D*>(target); mesh)
    {
      if(scene->HasMeshes())
        {
          uint8_t meshWarnings = 0;
          for(unsigned i = 0; i < scene->mNumMeshes; ++i)
            {
              auto mesh0 = scene->mMeshes[i];
              KawaiiAssimpMesh tmpMesh(mesh0, meshWarnings);
              if(mesh0->HasBones())
                qWarning().noquote() << QStringLiteral("KawaiiAssimp: ignored bones for mesh \"%1\"")
                                        .arg(QLatin1String(mesh0->mName.data, mesh0->mName.length));

              const size_t prevVertCount = mesh->vertexCount();

              bool needTr = !trMat.isIdentity();
              QMatrix4x4 normMat;
              if(needTr)
                normMat = QMatrix4x4(trMat.normalMatrix());

              if(needTr)
                tmpMesh.forallVerticesConst([mesh, &trMat, &normMat](const KawaiiPoint3D &p) {
                    mesh->addVertex(trMat * p.positionRef(), normMat.map(p.normalRef()), p.texCoordRef());
                  });
              else
                tmpMesh.forallVerticesConst([mesh](const KawaiiPoint3D &p) {
                    mesh->addVertex(p.positionRef(), p.normalRef(), p.texCoordRef());
                  });

              for(const auto &triangle: tmpMesh.getRawTriangles())
                {
                  mesh->addTriangle(
                        prevVertCount + triangle[0],
                      prevVertCount + triangle[1],
                      prevVertCount + triangle[2]);
                }
            }
          return true;
        } else
        return false;
    }

  auto resultScene = KawaiiRoot::getRoot<KawaiiScene>(target);
  auto resultModel = KawaiiRoot::getRoot<KawaiiModel3D>(target);

  if(!resultScene && !resultModel)
    return false;

  if(!resultModel)
    resultModel = resultScene->createChild<KawaiiModel3D>();

  bool needTr = !trMat.isIdentity();
  QMatrix4x4 normMat;
  if(needTr)
    normMat = QMatrix4x4(trMat.normalMatrix());

  if(scene->HasLights())
    {
      //todo: add light zygote class to core; create zygote objects here
      qWarning("KawaiiAssimp: illumination not implemented in plugin yet. Loaded scene may be incomplete!");
    }

  if(scene->HasCameras() && resultScene)
    for(unsigned i = 0; i < scene->mNumCameras; ++i)
      {
        auto *cam0 = scene->mCameras[i];

        auto *cam = resultScene->createChild<KawaiiCamera>();
        cam->setObjectName(qStr(cam0->mName));
        cam->setViewMat(glm::lookAt(glmVec3(cam0->mPosition), glmVec3(cam0->mLookAt), glmVec3(cam0->mUp)));
        if(needTr)
          cam->transform(trMat);
      }

  KawaiiSubsceneZygote *subscene = new KawaiiSubsceneZygote;
  subscene->moveToThread(target->thread());
  subscene->setParent(target);

  if(scene->HasMaterials())
    for(unsigned i = 0; i < scene->mNumMaterials; ++i)
      subscene->addMaterial() = readAiMaterial(scene->mMaterials[i], dataDir);


  KawaiiAssimpModelLoader modelLoader(*resultModel, scene);
  modelLoader.initSkeletonNodes();

  if(scene->HasMeshes())
    modelLoader.loadMeshes(subscene);

  if(needTr)
    modelLoader.transform(trMat, normMat);

  if(scene->HasMeshes())
    modelLoader.loadSkeleton();

  if(scene->HasAnimations())
    modelLoader.loadAnimations();

  resultModel->joinIdenticalVertices();

  return true;
}
