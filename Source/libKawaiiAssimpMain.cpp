#include "KawaiiAssimpLoader.hpp"
#include <Kawaii3D/KawaiiConfig.hpp>
#include <unordered_set>
#include <QMatrix4x4>
#include <QDebug>
#include <QUrl>

bool loadAssimpScene_fname(const QString &fileName, const QMatrix4x4 &trMat, KawaiiDataUnit *target)
{
  std::unique_ptr<KawaiiAssimpLoader> loader;
  if(!QFile::exists(fileName))
    {
      QUrl url(fileName);
      if(url.isLocalFile() && QFile::exists(url.toLocalFile()))
        loader = std::make_unique<KawaiiAssimpLoader>(url.toLocalFile());
      else
        return false;
    } else
    loader = std::make_unique<KawaiiAssimpLoader>(fileName);

  if(!loader->check())
    return false;

  return loader->load(target, trMat);
}

#ifdef ASSIMP_EARLY_LINK
#include <assimp/DefaultLogger.hpp>
namespace {
  class KawaiiAssimpLogger: public Assimp::Logger
  {
    std::unordered_set<Assimp::LogStream*> attStreams;

    // Logger interface
  public:
    bool attachStream(Assimp::LogStream *stream, [[maybe_unused]] unsigned int severity) override
    {
      if(!attStreams.count(stream))
        {
          attStreams.insert(stream);
          return true;
        } else
        return false;
    }

    bool detachStream(Assimp::LogStream *stream, [[maybe_unused]] unsigned int severity)
    {
      if(auto el = attStreams.find(stream); el != attStreams.end())
        {
          attStreams.erase(el);
          return true;
        } else
        return false;
    }
    bool detatchStream(Assimp::LogStream *stream, unsigned int severity)
    {
      return detachStream(stream, severity);
    }

  protected:
    void OnVerboseDebug(const char *message)
    {
#     ifdef KAWAIIASSIMP_DEBUG
      if(KawaiiConfig::getInstance().getDebugLevel() >= 3)
        qDebug().noquote() << "KawaiiAssimp:dbg: " << QString::fromLocal8Bit(message);
#     else
      static_cast<void>(message);
#     endif
    }

    void OnDebug(const char *message) override
    {
#     ifdef KAWAIIASSIMP_DEBUG
      if(KawaiiConfig::getInstance().getDebugLevel() >= 2)
        qDebug().noquote() << "KawaiiAssimp:dbg: " << QString::fromLocal8Bit(message);
#     else
      static_cast<void>(message);
#     endif
    }

    void OnInfo(const char *message) override
    {
#     ifdef KAWAIIASSIMP_DEBUG
      if(KawaiiConfig::getInstance().getDebugLevel() >= 2)
        qDebug().noquote() << "KawaiiAssimp:info: " << QString::fromLocal8Bit(message);
#     else
      static_cast<void>(message);
#     endif
    }

    void OnWarn(const char *message) override
    {
      if(KawaiiConfig::getInstance().getDebugLevel() >= 1)
        qWarning().noquote() << "KawaiiAssimp:warning: " << QString::fromLocal8Bit(message);
    }

    void OnError(const char *message) override
    {
      qCritical().noquote() << "KawaiiAssimp:error: " << QString::fromLocal8Bit(message);
    }
  };

  class libKawaiiAssimpMain
  {
    KawaiiDataUnit::LoadhookDescr assimpLoadHook;
  public:
    libKawaiiAssimpMain():
      assimpLoadHook(KawaiiDataUnit::LoadhookDescr(&loadAssimpScene_fname))
    {
      Assimp::DefaultLogger::set(new KawaiiAssimpLogger);
    }

    ~libKawaiiAssimpMain()
    {
      Assimp::DefaultLogger::kill();
    }
  };

  libKawaiiAssimpMain module;
}
#else
namespace {
  class libKawaiiAssimpMain
  {
    KawaiiDataUnit::LoadhookDescr assimpLoadHook;
  public:
    libKawaiiAssimpMain():
      assimpLoadHook(KawaiiDataUnit::LoadhookDescr(&loadAssimpScene_fname))
    { }

    ~libKawaiiAssimpMain() = default;
  };

  libKawaiiAssimpMain module;
}
#endif
