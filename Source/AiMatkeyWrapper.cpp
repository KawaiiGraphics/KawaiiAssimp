#include "AiMatkeyWrapper.h++"
#include <QHash>

namespace {
  constexpr std::string&& getKey(std::string &&arg0, int, int)
  {
    return std::move(arg0);
  }

  const QHash<std::string, KawaiiRenderParamRole> nonTexParams = {
    {getKey(AI_MATKEY_NAME), KawaiiRenderParamRole::Type},
    {getKey(AI_MATKEY_TWOSIDED), KawaiiRenderParamRole::Twosided},
    {getKey(AI_MATKEY_SHADING_MODEL), KawaiiRenderParamRole::ShadingModel},
    {getKey(AI_MATKEY_ENABLE_WIREFRAME), KawaiiRenderParamRole::EnableWireframe},
    {getKey(AI_MATKEY_BLEND_FUNC), KawaiiRenderParamRole::BlendFunc},
    {getKey(AI_MATKEY_OPACITY), KawaiiRenderParamRole::Opacity},
    {getKey(AI_MATKEY_BUMPSCALING), KawaiiRenderParamRole::Bumpscaling},
    {getKey(AI_MATKEY_SHININESS), KawaiiRenderParamRole::Shininess},
    {getKey(AI_MATKEY_REFLECTIVITY), KawaiiRenderParamRole::Reflectivity},
    {getKey(AI_MATKEY_SHININESS_STRENGTH), KawaiiRenderParamRole::ShininessStrength},
    {getKey(AI_MATKEY_REFRACTI), KawaiiRenderParamRole::Refracti},
    {getKey(AI_MATKEY_COLOR_DIFFUSE), KawaiiRenderParamRole::ColorDiffuse},
    {getKey(AI_MATKEY_COLOR_AMBIENT), KawaiiRenderParamRole::ColorAmbient},
    {getKey(AI_MATKEY_COLOR_SPECULAR), KawaiiRenderParamRole::ColorSpecular},
    {getKey(AI_MATKEY_COLOR_EMISSIVE), KawaiiRenderParamRole::ColorEmissive},
    {getKey(AI_MATKEY_COLOR_TRANSPARENT), KawaiiRenderParamRole::ColorTransparent},
    {getKey(AI_MATKEY_COLOR_REFLECTIVE), KawaiiRenderParamRole::ColorReflective},
    {getKey(AI_MATKEY_GLOBAL_BACKGROUND_IMAGE), KawaiiRenderParamRole::GlobalBackgroundImage}
  };

  const QHash<std::string, KawaiiTextureParamRole> texParams = {
    {_AI_MATKEY_TEXTURE_BASE, KawaiiTextureParamRole::FileName},
    {_AI_MATKEY_UVWSRC_BASE, KawaiiTextureParamRole::UvWSrc},
    {_AI_MATKEY_TEXOP_BASE, KawaiiTextureParamRole::Op},
    {_AI_MATKEY_MAPPING_BASE, KawaiiTextureParamRole::Mapping},
    {_AI_MATKEY_TEXBLEND_BASE, KawaiiTextureParamRole::Blend},
    {_AI_MATKEY_MAPPINGMODE_U_BASE, KawaiiTextureParamRole::MapmodeU},
    {_AI_MATKEY_MAPPINGMODE_V_BASE, KawaiiTextureParamRole::MapmodeV},
    {_AI_MATKEY_TEXMAP_AXIS_BASE, KawaiiTextureParamRole::MapAxis},
    {_AI_MATKEY_UVTRANSFORM_BASE, KawaiiTextureParamRole::UVtransform},
    {_AI_MATKEY_TEXFLAGS_BASE, KawaiiTextureParamRole::Flags}
  };
}

namespace std
{
  uint qHash(const std::string &str, uint seed = 0)
  {
    return ::qHash(QLatin1String(str.c_str(), str.length()), seed);
  }
}

AssimpParamKeyInfo trRenderParamKey(const std::string &key, int texType, size_t idx)
{
  if(auto i = nonTexParams.find(key); i != nonTexParams.end())
    return i.value();

  if(auto i = texParams.find(key); i != texParams.end())
    {
      KawaiiTextureRole role = KawaiiTextureRole::None;
      switch(texType)
        {
        case aiTextureType_DIFFUSE:
          role = KawaiiTextureRole::Diffuse;
          break;

        case aiTextureType_SPECULAR:
          role = KawaiiTextureRole::Specular;
          break;

        case aiTextureType_AMBIENT:
          role = KawaiiTextureRole::Ambient;
          break;

        case aiTextureType_EMISSIVE:
          role = KawaiiTextureRole::Emissive;
          break;

        case aiTextureType_HEIGHT:
          role = KawaiiTextureRole::Height;
          break;

        case aiTextureType_NORMALS:
          role = KawaiiTextureRole::Normals;
          break;

        case aiTextureType_SHININESS:
          role = KawaiiTextureRole::Shininess;
          break;

        case aiTextureType_OPACITY:
          role = KawaiiTextureRole::Opacity;
          break;

        case aiTextureType_DISPLACEMENT:
          role = KawaiiTextureRole::Displacement;
          break;

        case aiTextureType_LIGHTMAP:
          role = KawaiiTextureRole::Lightmap;
          break;

        case aiTextureType_REFLECTION:
          role = KawaiiTextureRole::Reflection;
          break;

        case aiTextureType_UNKNOWN:
          role = KawaiiTextureRole::Unknown;
          break;
        }
      return std::tuple(role, *i, idx);
    }

  return std::monostate {};
}
