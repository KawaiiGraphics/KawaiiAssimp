#ifndef KAWAIIASSIMPMODELLOADER_H
#define KAWAIIASSIMPMODELLOADER_H

#include <Kawaii3D/AssetZygote/KawaiiSubsceneZygote.hpp>
#include <Kawaii3D/Geometry/KawaiiModel3D.hpp>
#include "KawaiiAssimpNode.h++"

class aiScene;
class aiNode;

class KawaiiAssimpModelLoader
{
public:
  KawaiiAssimpModelLoader(KawaiiModel3D &target, const aiScene *scene);

  void initSkeletonNodes();

  void loadMeshes(KawaiiSubsceneZygote *subscene);

  void transform(const QMatrix4x4 &trMat, const QMatrix4x4 &normMatrix);

  void loadSkeleton();

  void loadAnimations();




  //IMPLEMENT
private:
  KawaiiModel3D &target;
  const aiScene *scene;

  struct Skeleton {
    std::list<KawaiiAssimpNode> topLevelNodes;
    std::unordered_map<std::string, KawaiiAssimpNode*> allNodes;

    void removeRedundant();
  };

  Skeleton skeleton;

  std::vector<KawaiiAssimpMesh*> createdMeshes;
};

#endif // KAWAIIASSIMPMODELLOADER_H
