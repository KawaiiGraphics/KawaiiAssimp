#include "KawaiiAssimpModelLoader.h++"
#include "KawaiiAssimpMesh.h++"
#include "Util.h++"

#include <Kawaii3D/Geometry/KawaiiSkeletalAnimation.hpp>
#include <assimp/scene.h>
#include <QDebug>

KawaiiAssimpModelLoader::KawaiiAssimpModelLoader(KawaiiModel3D &target, const aiScene *scene):
  target(target),
  scene(scene)
{
}

void KawaiiAssimpModelLoader::initSkeletonNodes()
{
  using namespace util;
  auto *rootNode = scene->mRootNode;

  for(uint32_t i = 0; i < rootNode->mNumChildren; ++i)
    {
      skeleton.topLevelNodes.emplace_back(stdStr(rootNode->mChildren[i]->mName), trMat(rootNode->mChildren[i]->mTransformation));
      skeleton.allNodes[skeleton.topLevelNodes.back().name] = &skeleton.topLevelNodes.back();

      struct State {
        KawaiiAssimpNode *parent;
        aiNode *current;

        inline State(aiNode *node, KawaiiAssimpNode *parent = nullptr): parent(parent), current(node) {}
      };

      std::list<State> nodes(rootNode->mChildren[i]->mChildren,
                             rootNode->mChildren[i]->mChildren + rootNode->mChildren[i]->mNumChildren);

      for(auto &j: nodes)
        j.parent = &skeleton.topLevelNodes.back();

      while(!nodes.empty())
        {
          const State j = nodes.front();
          nodes.pop_front();

          j.parent->children.emplace_back(stdStr(j.current->mName), trMat(j.current->mTransformation));
          auto created = &j.parent->children.back();
          skeleton.allNodes[created->name] = created;
          created->parent = j.parent;

          for(uint32_t k = 1; k <= j.current->mNumChildren; ++k)
            nodes.emplace_front(j.current->mChildren[j.current->mNumChildren - k], created);
        }
    }
}

void KawaiiAssimpModelLoader::loadMeshes(KawaiiSubsceneZygote *subscene)
{
  uint8_t meshWarnings = 0;
  createdMeshes.reserve(scene->mNumMeshes);
  for(unsigned i = 0; i < scene->mNumMeshes; ++i)
    {
      auto mesh0 = scene->mMeshes[i];
      KawaiiAssimpMesh *mesh = target.createChild<KawaiiAssimpMesh>(mesh0, meshWarnings);

      if(mesh0->HasBones())
        mesh->loadBones(skeleton.allNodes);

      if(mesh0->mMaterialIndex < scene->mNumMaterials)
        subscene->addMeshInstance(*mesh, subscene->getMaterial(mesh0->mMaterialIndex));

      createdMeshes.push_back(mesh);
    }
  skeleton.removeRedundant();
}

void KawaiiAssimpModelLoader::transform(const QMatrix4x4 &trMat, const QMatrix4x4 &normMatrix)
{
  for(auto *i: createdMeshes)
    i->transform(trMat, normMatrix);
}

void KawaiiAssimpModelLoader::loadSkeleton()
{
  const auto skeletonName = util::stdStr(scene->mRootNode->mName);
  const auto rootMat = util::trMat(scene->mRootNode->mTransformation.Inverse().Transpose());
  auto &createdSkeleton = target.createSkeleton(skeletonName, rootMat);

  struct State {
    KawaiiSkeleton3D::Node *parent;
    KawaiiAssimpNode *current;

    inline State(KawaiiAssimpNode &node): parent(nullptr), current(&node) {}
    inline State(KawaiiAssimpNode &node, KawaiiSkeleton3D::Node *parent): parent(parent), current(&node) {}
  };

  std::list<State> nodes(skeleton.topLevelNodes.begin(), skeleton.topLevelNodes.end());
  for(auto &i: nodes)
    i.parent = &createdSkeleton.getRoot();

  while(!nodes.empty())
    {
      const State i = nodes.front();
      nodes.pop_front();

      KawaiiSkeleton3D::Node *createdNode = &i.parent->createChild(i.current->tr, i.current->name);

      for(auto &j: i.current->bones)
        {
          size_t boneIndex = createdNode->createBone();
          auto *bone = createdNode->getSkeleton()->getBone(boneIndex);
          bone->setTransform(j.tr);
          bone->setInfluence(std::move(j.influence));
          bone->setObjectName(j.name);
          j.mesh->assignBone(boneIndex);
        }

      for(auto j = i.current->children.rbegin(); j != i.current->children.rend(); ++j)
        nodes.emplace_front(*j, createdNode);
    }

  target.enableSkeleton();
}

namespace {
  aiQuaternion interpolate(const aiQuaternion &a, const aiQuaternion &b, float factor)
  {
    aiQuaternion out;
    aiQuaternion::Interpolate(out, a, b, factor);
    return out;
  }

  aiVector3D interpolate(const aiVector3D &a, const aiVector3D &b, float factor)
  {
    return b*factor + a * (1.0f - factor);
  }

  template<typename T>
  static T findValueByTime(float time, const T *v, uint32_t N)
  {
    if(N == 1)
      return *v;

    auto el = std::find_if(v, v + N, [time] (const T &i) { return time <= i.mTime; } );
    if(el == v + N)
      return el[N-1];

    if(el == v)
      return *el;

    auto el0 = el - 1;

    float deltaTime = el->mTime - el0->mTime;
    float factor = (time - el0->mTime) / deltaTime;
    assert(factor >= 0.0 && factor <= 1.0);

    return T(time, interpolate(el0->mValue, el->mValue, factor));
  }

  float gcd_f(float a, float b, float epsilon)
  {
    float t;
    while (b > epsilon)
      {
        t = b;
        b = std::fmod(a, b);
        a = t;
      }
    return a;
  }
}

void KawaiiAssimpModelLoader::loadAnimations()
{
//  qWarning("KawaiiAssimp: animations not implemented yet. Loaded scene may be incomplete!");
  for(uint32_t i = 0; i < scene->mNumAnimations; ++i)
    {
      const auto *anim0 = scene->mAnimations[i];

      bool hasMeshAnim = anim0->mNumMeshChannels > 0,
          hasMorphAnim = anim0->mNumMorphMeshChannels > 0;

      if(hasMeshAnim)
        qWarning().noquote() << QStringLiteral("KawaiiAssimp: Warning: Animation \"%1\" has %2 mesh animate channels. "
                                               "Mesh animate channels are not implemented yet!")
                                .arg(util::qStr(anim0->mName), QString::number(anim0->mNumMeshChannels));

      if(hasMorphAnim)
        qWarning().noquote() << QStringLiteral("KawaiiAssimp: Warning: Animation \"%1\" has %2 morph animate channels. "
                                               "Morph animate channels are not implemented yet!")
                                .arg(util::qStr(anim0->mName), QString::number(anim0->mNumMorphMeshChannels));

      if(!anim0->mNumChannels)
        {
          qWarning().noquote() << QStringLiteral("KawaiiAssimp: Warning: Animation \"%1\" skiped!").arg(util::qStr(anim0->mName));
          continue;
        }

      const float ticksPerSecond0 = anim0->mTicksPerSecond?
            anim0->mTicksPerSecond:
            25.0f;

      const float durationSec = anim0->mDuration / ticksPerSecond0;

      auto *anim = target.createChild<KawaiiSkeletalAnimation>();
      anim->setObjectName(util::qStr(anim0->mName));
      for(uint32_t j = 0; j < anim0->mNumChannels; ++j)
        {
          const auto *channel0 = anim0->mChannels[j];
          const std::string nodeName = util::stdStr(channel0->mNodeName);

          float minDeltaSec = 0;
          for(uint32_t k = 0; k < channel0->mNumRotationKeys; ++k)
            minDeltaSec = gcd_f(channel0->mRotationKeys[k].mTime / ticksPerSecond0, minDeltaSec, 0.001);

          for(uint32_t k = 0; k < channel0->mNumScalingKeys; ++k)
            minDeltaSec = gcd_f(channel0->mScalingKeys[k].mTime / ticksPerSecond0, minDeltaSec, 0.001);

          for(uint32_t k = 0; k < channel0->mNumPositionKeys; ++k)
            minDeltaSec = gcd_f(channel0->mPositionKeys[k].mTime / ticksPerSecond0, minDeltaSec, 0.001);

          const float outTicksPerSec = 1.0f / minDeltaSec;

          std::vector<KawaiiSkeletalAnimation::Transform> keys;
          keys.reserve(std::ceil(durationSec / minDeltaSec));
          for(float t = 0; t <= durationSec; t += minDeltaSec)
            {
              const float animTime = t * ticksPerSecond0;
              const auto rot = findValueByTime(animTime, channel0->mRotationKeys, channel0->mNumRotationKeys).mValue;
              const auto pos = findValueByTime(animTime, channel0->mPositionKeys, channel0->mNumPositionKeys).mValue;
              const auto scale = findValueByTime(animTime, channel0->mScalingKeys, channel0->mNumScalingKeys).mValue;

              keys.emplace_back(glm::quat(rot.w, rot.x, rot.y, rot.z), glm::vec3(pos.x, pos.y, pos.z), glm::vec3(scale.x, scale.y, scale.z));
            }
          anim->setChannel(nodeName, KawaiiSkeletalAnimation::Channel(std::move(keys), outTicksPerSec));
        }
    }
}

namespace {
  template<typename Container, typename T>
  void removeOne(Container &c, const T *val)
  {
    auto findRefFunc = [val] (const T &a) { return &a == val; };

    if(auto el = std::find_if(c.begin(), c.end(), findRefFunc); el != c.end())
      c.erase(el);
  }
}

void KawaiiAssimpModelLoader::Skeleton::removeRedundant()
{
  for(auto i = allNodes.begin(); i != allNodes.end();)
    {
      if(!i->second->bones.empty() || !i->second->children.empty())
        ++i;
      else
        {
          if(i->second->parent)
            removeOne(i->second->parent->children, i->second);
          else
            removeOne(topLevelNodes, i->second);

          i = allNodes.erase(i);
        }
    }
}
