#ifndef KAWAIIASSIMPNODE_H
#define KAWAIIASSIMPNODE_H

#include <list>
#include <string>
#include <vector>
#include <glm/mat4x4.hpp>
#include "KawaiiAssimpBone.h++"

struct KawaiiAssimpNode
{
  KawaiiAssimpNode(const std::string &name, const glm::mat4 &tr);

  std::string name;
  glm::mat4 tr;
  std::vector<KawaiiAssimpBone> bones;
  std::list<KawaiiAssimpNode> children;
  KawaiiAssimpNode *parent;
};

#endif // KAWAIIASSIMPNODE_H
