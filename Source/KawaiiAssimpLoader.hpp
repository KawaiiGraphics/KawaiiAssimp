#ifndef KAWAIIASSIMPLOADER_HPP
#define KAWAIIASSIMPLOADER_HPP

#include <Kawaii3D/KawaiiDataUnit.hpp>
#include <functional>
#include <QIODevice>
#include <QDir>

#include "KawaiiAssimp_global.hpp"

struct aiScene;

class KAWAIIASSIMP_SHARED_EXPORT KawaiiAssimpLoader
{
public:
  KawaiiAssimpLoader(QIODevice &io, const QDir &dataDir);
  KawaiiAssimpLoader(QIODevice &io);
  KawaiiAssimpLoader(QIODevice &&io);
  KawaiiAssimpLoader(const QString &fileName);
  ~KawaiiAssimpLoader();

  bool check() const;

  bool load(KawaiiDataUnit *target, const QMatrix4x4 &trMat);



  //IMPLEMENT
private:
  static KawaiiDataUnit::LoadhookDescr assimpLoadHook;

  QDir dataDir;
  const aiScene *scene;
};

#endif // KAWAIIASSIMPLOADER_HPP
