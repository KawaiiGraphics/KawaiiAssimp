#include "KawaiiAssimpBone.h++"

KawaiiAssimpBone::KawaiiAssimpBone(const QString &name, const glm::mat4 &tr, std::vector<KawaiiBone3D::Influence> &&influence, KawaiiAssimpMesh *mesh):
  name(name),
  tr(tr),
  influence(std::move(influence)),
  mesh(mesh)
{
}
