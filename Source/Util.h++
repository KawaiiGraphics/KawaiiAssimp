#ifndef UTIL_HPP
#define UTIL_HPP

#include <assimp/types.h>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <QVector4D>
#include <QVector3D>
#include <QString>

namespace util {
  template<typename T>
  glm::vec3 glmVec3(const T &origin)
  {
    return glm::vec3(origin.x, origin.y, origin.z);
  }

  template<typename T>
  QVector3D qVec3(const T &origin)
  {
    return QVector3D(origin.x, origin.y, origin.z);
  }

  template<typename T>
  QVector4D qVec4(const T &origin)
  {
    return QVector4D(origin.x, origin.y, origin.z, 1.0);
  }

  inline QString qStr(const aiString &origin)
  {
    QByteArray array(origin.data, origin.length);
    return QString(array);
  }

  inline std::string stdStr(const aiString &origin)
  {
    return std::string(origin.data, origin.length);
  }

  inline glm::mat4 trMat(const aiMatrix4x4t<float> &origin)
  {
    glm::mat4 trMat;
    for(size_t x = 0; x < 4; ++x)
      for(size_t y = 0; y < 4; ++y)
        trMat[x][y] = origin[y][x];

    return trMat;
  }
}

#endif // UTIL_HPP
